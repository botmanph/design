import React from "react";
import { withKnobs } from "@storybook/addon-knobs";
import centered from "@storybook/addon-centered/react";
import Typography from "./Typography";

export default {
  title: "Typography",
  component: Typography,
  decorators: [withKnobs, centered],
};

export const info = () => <Typography>Lorem Ipsum</Typography>;

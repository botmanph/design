import * as React from "react";
import classNames from "classnames";

export type TypographyProps = {
  size?:
    | "xs"
    | "sm"
    | "base"
    | "lg"
    | "xl"
    | "2xl"
    | "3xl"
    | "4xl"
    | "5xl"
    | "6xl";
  className?: string;
  italic?: boolean;
  weight?:
    | "hairline"
    | "thin"
    | "light"
    | "normal"
    | "medium"
    | "semibold"
    | "bold"
    | "extrabold"
    | "black";
  responsive?: {
    sm?: {
      weight?:
        | "hairline"
        | "thin"
        | "light"
        | "normal"
        | "medium"
        | "semibold"
        | "bold"
        | "extrabold"
        | "black";
    };
    md: {
      weight?:
        | "hairline"
        | "thin"
        | "light"
        | "normal"
        | "medium"
        | "semibold"
        | "bold"
        | "extrabold"
        | "black";
    };
    lg: {
      weight?:
        | "hairline"
        | "thin"
        | "light"
        | "normal"
        | "medium"
        | "semibold"
        | "bold"
        | "extrabold"
        | "black";
    };
    xl: {
      weight?:
        | "hairline"
        | "thin"
        | "light"
        | "normal"
        | "medium"
        | "semibold"
        | "bold"
        | "extrabold"
        | "black";
    };
  };
};

export const Typography: React.FC<TypographyProps> = ({
  size,
  className,
  italic,
  weight,
  ...props
}: TypographyProps) => {
  return (
    <p
      {...props}
      className={classNames(
        "text-base",
        {
          [`text-${size}`]: size,
          [`font-${weight}`]: weight,
          italic,
        },
        className
      )}
    />
  );
};

Typography.defaultProps = {
  size: "base",
  className: "",
};
export default Typography;

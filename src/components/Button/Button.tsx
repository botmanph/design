import * as React from "react";
import { get } from "lodash";
import classNames from "classnames";

export type ButtonProps = {
  /**
   * Button context
   */
  context?: "primary" | "info" | "warning" | "danger" | "default";
  /**
   * Variant
   */
  onClick?: (e: React.MouseEvent<HTMLButtonElement>) => void;
  children: React.ReactNode;
  className?: string;
  type?: "submit" | "reset";
};

export const Button: React.FC<ButtonProps> = ({
  context,
  className,
  ...props
}: ButtonProps) => {
  const colors = {
    primary: {
      bg: "bg-teal-500",
      hover: "bg-teal-700",
    },
    info: {
      bg: "bg-blue-500",
      hover: "bg-blue-700",
    },
    warning: {
      bg: "bg-yellow-500",
      hover: "bg-yellow-700",
    },
    danger: {
      bg: "bg-red-700",
      hover: "bg-red-800",
    },
    default: {
      bg: "bg-white",
      hover: "bg-gray-100",
    },
  };
  return (
    <button
      {...props}
      className={classNames(
        "text-white font-bold py-2 px-4 border-gray-400 shadow rounded",
        {
          [`${get(colors, [context as string, "bg"])}`]: context,
          [`hover:${get(colors, [context as string, "hover"])}`]: context,
          [`text-gray-800`]: context === "default",
        },
        className
      )}
    />
  );
};

Button.defaultProps = {
  context: "default",
  className: "",
};

export default Button;

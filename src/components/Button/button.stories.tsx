import React from "react";
import { withKnobs, boolean } from "@storybook/addon-knobs";
import Button from "./Button";

export default {
  title: "Button",
  component: Button,
  decorators: [withKnobs],
};

export const preview = () => (
  <React.Fragment>
    <Button context="default">Lorem Ipsum</Button>
    <Button context="primary">Lorem Ipsum</Button>
    <Button context="info">Lorem Ipsum</Button>
    <Button context="warning">Lorem Ipsum</Button>
    <Button context="danger">Lorem Ipsum</Button>
  </React.Fragment>
);

export const Default = () => <Button context="default">Lorem Ipsum</Button>;
export const Primary = () => <Button context="primary">Lorem Ipsum</Button>;
export const Info = () => <Button context="info">Lorem Ipsum</Button>;
export const Danger = () => <Button context="danger">Lorem Ipsum</Button>;

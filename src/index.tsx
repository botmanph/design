import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import firebase from "./utils/firebase";
import "./styles/tailwind.css";
import * as serviceWorker from "./serviceWorker";

firebase.initializeApp({
  apiKey: "AIzaSyAu6rLfEL5zmK3xyx_jf8JkOTWK5lhQzn4",
  authDomain: "auth.botman.ph",
  databaseURL: "https://botmanph.firebaseio.com",
  projectId: "botmanph",
  storageBucket: "botmanph.appspot.com",
  messagingSenderId: "660995801326",
  appId: "1:660995801326:web:5ddf7f37879c1c019d33ff",
  measurementId: "G-17787E73BQ",
});

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById("root")
);

serviceWorker.unregister();

import { ActionMap } from '../utils'

export enum Types {
	MAKE_AUTH = "MAKE_AUTH"
}

type Payload = {
	[Types.MAKE_AUTH]: undefined
}


export type State = {
	auth: boolean;
}

export type Actions = ActionMap<Payload>[keyof ActionMap<Payload>]

export function reducer(state: State, action: Actions): State {

	switch (action.type) {
		case "MAKE_AUTH":
			return {
				...state,
				auth: true
			}

		default:
			return state
	}

}
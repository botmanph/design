import React, { Dispatch } from "react";
import firebase from "../../utils/firebase";
import { State, reducer, Actions, Types } from "./reducer";
export { Types } from "./reducer";


const initState: State = {
  auth: false,
};

export const AuthProviderCtx = React.createContext<{
  state: State;
  dispatch: Dispatch<Actions>;
}>({ state: initState, dispatch: () => null });

export type AuthProviderProps = {
  children: React.ReactNode;
};

export function AuthProvider(props: AuthProviderProps): React.ReactElement {
  const [state, dispatch] = React.useReducer(reducer, initState);

  React.useEffect(() => {
    (async () => {
      const userId = firebase.auth().currentUser?.uid;
      userId &&
        dispatch({
          type: Types.MAKE_AUTH,
        });
    })();
  }, []);

  return (
    <AuthProviderCtx.Provider
      value={{
        state,
        dispatch,
      }}
    >
      {props.children}
    </AuthProviderCtx.Provider>
  );
}

export default AuthProvider;

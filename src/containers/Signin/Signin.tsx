import React from "react";
import firebase from "../../utils/firebase";

export function Signin() {
  function handleSignin() {
    const provider = new firebase.auth.FacebookAuthProvider();
    provider.addScope("user_birthday");
    firebase
      .auth()
      .signInWithPopup(provider)
      .then(function (result) {
        console.log(result);
        // ...
      })
      .catch(function (error) {
        console.log(error);
      });
  }
  return (
    <div>
      <button onClick={handleSignin}>Click</button>
    </div>
  );
}

export default Signin;

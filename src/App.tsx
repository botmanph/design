import React from "react";
import { BrowserRouter, Route } from "react-router-dom";
import Dashboard from "./containers/Dashboard";
import Signin from "./containers/Signin";
import AuthProvider from "./providers/AuthProvider";

export function App() {
  return (
    <AuthProvider>
      <BrowserRouter>
        <Route exact path="/" component={Dashboard} />
        <Route path="/signin" component={Signin} />
      </BrowserRouter>
    </AuthProvider>
  );
}

export default App;

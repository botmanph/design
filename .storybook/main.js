module.exports = {
  addons: [
    "@storybook/preset-create-react-app",
    "@storybook/addon-docs",
    "@storybook/addon-actions/register",
    "@storybook/addon-knobs/register",
    "@storybook/addon-viewport/register"
  ],
  stories: ["../src/components/**/*.stories.(tsx|mdx)"],
};
